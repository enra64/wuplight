plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("plugin.serialization") version "1.4.10"
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("kotlin-android")
    id("androidx.navigation.safeargs.kotlin")
    id("com.google.android.gms.oss-licenses-plugin")
}

android {
    compileSdkVersion(30)
    val appVersionCode = Integer.valueOf(System.getenv("BUILD_NUMBER") ?: "4")
    defaultConfig {
        applicationId = "de.oerntec.wuplight"
        minSdkVersion(28)
        targetSdkVersion(30)
        versionCode = appVersionCode
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    signingConfigs {
        create("release") {
            storeFile = file("/tmp/keystore.keystore")
            storePassword = System.getenv("CM_KEYSTORE_PASSWORD")
            keyAlias = System.getenv("CM_KEY_ALIAS_USERNAME")
            keyPassword = System.getenv("CM_KEY_ALIAS_PASSWORD")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildFeatures {
        viewBinding = true
    }
    kotlinOptions {
        this.jvmTarget = "1.8"
    }
}

repositories {
    mavenCentral()
    google()
    gradlePluginPortal()

    maven("https://kotlin.bintray.com/kotlinx")
    //etc
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.3.1")
    implementation("androidx.coordinatorlayout:coordinatorlayout:1.1.0")
    val kotlinVersion = "1.4.10"
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")

    implementation("androidx.core:core-ktx:1.6.0")
    implementation("androidx.appcompat:appcompat:1.3.0")
    implementation("com.google.android.material:material:1.5.0-alpha01")

    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("androidx.preference:preference-ktx:1.1.1")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1")

    implementation("androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03")
    kapt("androidx.hilt:hilt-compiler:1.0.0")
    implementation("com.google.dagger:hilt-android:2.33-beta")
    kapt("com.google.dagger:hilt-android-compiler:2.33-beta")
    implementation("com.google.android.gms:play-services-oss-licenses:17.0.0")


    implementation("androidx.navigation:navigation-fragment-ktx:2.3.5")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.5")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1") // JVM dependency

    implementation("me.relex:circleindicator:2.1.4")
    implementation("com.airbnb.android:lottie:3.5.0")
    implementation("com.tomerrosenfeld.customanalogclockview:custom-analog-clock-view:1.1")
    implementation("com.github.Chrisvin:RubberPicker:v1.5")
    implementation("org.zeromq:jeromq:0.5.2")
}