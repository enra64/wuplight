package de.oerntec.wuplight.data.objs

data class OnboardingSlide(
    val title: String,
    val description: String,
    val animation: String
)
