package de.oerntec.wuplight.data.repositories

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import de.oerntec.wuplight.R
import de.oerntec.wuplight.data.objs.Alarm
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

class AlarmRepository @Inject constructor(@ApplicationContext private val context: Context) {
    private val sharedPreferences: SharedPreferences
        get() = context.getSharedPreferences(
            context.getString(R.string.PREFS_ALARMS),
            Context.MODE_PRIVATE
        )

    fun setAlarm(alarm: Alarm): Alarm {
        sharedPreferences
            .edit()
            .putString(
                alarm.id,
                Json.encodeToString(alarm)
            )
            .apply()
        return alarm
    }

    fun removeAlarm(id: String) {
        sharedPreferences
            .edit()
            .remove(id)
            .apply()
    }

    fun getAlarms(): List<Alarm> {
        val alarmsUnsorted: List<Alarm> = sharedPreferences.all.map { Json.decodeFromString(it.value as String) }
        val alarms = alarmsUnsorted.sortedWith(compareBy { it.hour * 60 + it.minute })
        val outDatedAlarms = alarms.filter { it.isActive && it.hasRung() }

        return when {
            outDatedAlarms.size > 1 -> {
                throw AssertionError("More than 1 alarms is active?")
            }
            outDatedAlarms.size == 1 -> {
                outDatedAlarms.forEach { setAlarm(it.copy(isActive = false, wasSetActiveAt = 0)) }
                getAlarms()
            }
            else -> {
                alarms
            }
        }
    }
}