package de.oerntec.wuplight.data.repositories

import android.content.Context
import android.content.Context.MODE_PRIVATE
import dagger.hilt.android.qualifiers.ApplicationContext
import de.oerntec.wuplight.R
import de.oerntec.wuplight.networking.LedMatrix
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

class DefaultWupLightRepository @Inject constructor(@ApplicationContext private val context: Context) {
    fun saveDefaultWupLight(matrix: LedMatrix) {
        context
            .getSharedPreferences(context.getString(R.string.PREFS_DEFAULT_WUPLIGHT), MODE_PRIVATE)
            .edit()
            .putString(
                context.getString(R.string.PREF_KEY_DEFAULT_WUPLIGHT),
                Json.encodeToString(matrix)
            )
            .apply()
    }

    fun getDefaultWupLight(): LedMatrix? {
        val savedValue = context
            .getSharedPreferences(context.getString(R.string.PREFS_DEFAULT_WUPLIGHT), MODE_PRIVATE)
            .getString(context.getString(R.string.PREF_KEY_DEFAULT_WUPLIGHT), null)

        return if (savedValue.isNullOrBlank()) {
            null
        } else {
            Json.decodeFromString(savedValue)
        }
    }
}