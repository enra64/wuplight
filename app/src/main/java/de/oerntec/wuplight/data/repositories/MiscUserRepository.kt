package de.oerntec.wuplight.data.repositories

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import de.oerntec.wuplight.R
import javax.inject.Inject

class MiscUserRepository @Inject constructor(@ApplicationContext private val context: Context) {
    private val sharedPreferences: SharedPreferences
        get() = context.getSharedPreferences(
            context.getString(R.string.PREFS_MISC_USER),
            Context.MODE_PRIVATE
        )

    fun setOnboardingFinished() {
        sharedPreferences
            .edit()
            .putBoolean(
                "onboarding_is_finished",
                true
            )
            .apply()
    }

    fun isOnboardingFinished(): Boolean {
        return sharedPreferences.getBoolean("onboarding_is_finished", false)
    }
}