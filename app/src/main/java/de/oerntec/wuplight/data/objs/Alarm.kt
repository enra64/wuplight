package de.oerntec.wuplight.data.objs

import android.os.Parcelable
import de.oerntec.wuplight.networking.WupLightSetTime
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.Serializable
import java.util.*

@Parcelize
@Serializable
/**
 * @param wasSetActiveAt unix timestamp, milliseconds, UTC. 0 is default/invalid
 */
data class Alarm(
    val id: String = UUID.randomUUID().toString(),
    val hour: Int,
    val minute: Int,
    val startColorTemperature: Int,
    val endColorTemperature: Int,
    val blendInDuration: Int,
    val isActive: Boolean,
    val wasSetActiveAt: Long
) : Parcelable {
    /**
     * Return true if the alarm has already rung
     */
    fun hasRung(): Boolean {
        if (!isActive) {
            return false
        }

        val nowCalendar = Calendar.getInstance()
        val alarmCalendar = Calendar.getInstance()
        alarmCalendar.timeInMillis = wasSetActiveAt

        val alarmSetDay = alarmCalendar.get(Calendar.DAY_OF_YEAR)
        val nowDay = nowCalendar.get(Calendar.DAY_OF_YEAR)

        return if (alarmSetDay < nowDay) {
            true
        } else {
            val alarmTime = alarmCalendar.get(Calendar.HOUR_OF_DAY) * 3600 + alarmCalendar.get(Calendar.MINUTE) * 60 + alarmCalendar.get(Calendar.SECOND)
            val nowTime = nowCalendar.get(Calendar.HOUR_OF_DAY) * 3600 + nowCalendar.get(Calendar.MINUTE) * 60 + nowCalendar.get(Calendar.SECOND)

            alarmTime > nowTime
        }
    }
}

fun alarmToCommand(alarm: Alarm): WupLightSetTime {
    return WupLightSetTime(
        alarm.hour,
        alarm.minute,
        TimeZone.getDefault().id,
        alarm.blendInDuration,
        alarm.startColorTemperature,
        alarm.endColorTemperature
    )
}