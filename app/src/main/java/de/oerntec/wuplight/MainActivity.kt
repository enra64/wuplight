package de.oerntec.wuplight

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import de.oerntec.wuplight.data.repositories.MiscUserRepository
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var userInfoRepository: MiscUserRepository
    private var navigateUpCallback: (() -> Boolean)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

    fun showToolbar(show: Boolean = true) {
        if (show) {
            toolbar.visibility = View.VISIBLE
        } else {
            toolbar.visibility = View.GONE
        }
    }

    fun setToolbarTitle(title: String?) {
        title?.let {
            toolbar?.title = title
        } ?: run {
            toolbar?.title = getString(R.string.app_name)
        }
    }

    fun setToolbarTranslucent(translucent: Boolean) {
        val toolbarColor = if (translucent) {
            R.color.toolbarColorTranslucent
        } else {
            R.color.toolbarColorOpaque
        }
        val statusBarColor = if (translucent) {
            R.color.statusBarColorTranslucentToolbar
        } else {
            R.color.statusBarColorOpaqueToolbar
        }
        toolbar?.setBackgroundColor(ContextCompat.getColor(this, toolbarColor))
        window.statusBarColor = ContextCompat.getColor(this, statusBarColor)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigateUpCallback?.invoke() ?: false
    }

    fun setToolbarHomeUpEnabled(enabled: Boolean, callback: (() -> Boolean)? = null) {
        supportActionBar?.setDisplayHomeAsUpEnabled(enabled)
        navigateUpCallback = callback
    }

    fun showSnackbar(text: String, buttonText: String? = null, action: (() -> Unit)? = null) {
        val duration = action?.let { LENGTH_LONG } ?: kotlin.run { LENGTH_SHORT }
        val snackbar = Snackbar.make(main_coordinator_layout, text, duration)
        action?.let {
            if (buttonText != null) {
                snackbar.setAction(buttonText) { it() }
            } else {
                throw RuntimeException("action given but not buttonText")
            }
        }

        snackbar.show()
    }

    fun showFab(show: Boolean) {
        if (show) {
            floating_action_button?.visibility = View.VISIBLE
        } else {
            floating_action_button?.visibility = View.GONE
        }
    }

    fun setFabOnClickListener(listener: View.OnClickListener) {
        floating_action_button?.setOnClickListener(listener)
    }
}