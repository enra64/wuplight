package de.oerntec.wuplight

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WupLightApplication : Application()