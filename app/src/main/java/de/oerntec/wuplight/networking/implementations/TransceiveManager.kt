package de.oerntec.wuplight.networking.implementations

import android.os.Handler
import android.os.Looper
import android.util.Log
import de.oerntec.wuplight.networking.*
import de.oerntec.wuplight.networking.interfaces.*
import kotlinx.serialization.json.JsonElement
import org.zeromq.ZContext
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import kotlin.concurrent.schedule

/**
 * This is the class to use if you want to connect to a matrix
 *
 * @param matrix The [LedMatrix] we want to connect to
 * @param listener This [ScriptListener] instance is used to react to common matrix data events
 * @param connectionStatusListener The [ConnectionStatusListener] we want to inform about significant connection state changes
 * @param installationId The installation id of this app
 */
class TransceiveManager(
    private val installationId: String,
    private val matrix: LedMatrix,
    private val listener: ScriptListener,
    private val connectionStatusListener: ConnectionStatusListener
) : Connection, ScriptSender {
    /**
     * The constant connection module currently in use
     */
    private var module: MatrixTransceiveModule? = null

    /**
     * Queue for outgoing messages
     */
    private val outBox = ArrayBlockingQueue<Message>(200)

    /**
     * The ZMQ context
     */
    private val zContext = ZContext(1)

    /**
     * The connection string that can be used to connect to the matrix
     */
    private val connectionString: String

    /**
     * True if the context has been terminated by this class
     */
    private var zmqContextTerminated = false

    /**
     * the number of tries we have now done to reconnect to the matrix
     */
    private var currentCycleIteration = 0

    /**
     * the number of connection tries this transceivemanager has done. Designed as a failsafe
     */
    private var overallConnectionAttempts = 0

    /**
     * If this is true, we will try to cycle the module and restart the connection, as long as we don't have more than
     * MAXIMUM_CYCLE_COUNT tries already.
     *
     */
    private var retryConnection = true

    /**
     * Timer instance used for connection request timeout and retry timing
     */
    private var timer = Timer("TransceiveManagerTimer")

    /**
     * List of connection timeout tasks
     */
    private var connectionRequestTask: TimerTask? = null

    init {
        val isIPv6 = matrix.address.contains(":")
        connectionString = if (isIPv6) {
            "tcp://[${matrix.address}]:${matrix.dataPort}"
        } else {
            "tcp://${matrix.address}:${matrix.dataPort}"
        }
    }

    override fun initiateConnection() {
        cycleModule()
        sendMessage(ConnectionRequest())
        connectionRequestTask?.cancel()
        connectionRequestTask = timer.schedule(5000) {
            connectionStatusListener.onConnectionRequestTimeout(matrix)
        }
    }

    override fun sendMessage(message: Message) {
        if (message.message_type.isBlank()) {
            throw AssertionError("Messages must have message_type")
        }
        message.id = installationId

        outBox.add(message)
    }

    /**
     * Close the connection, free resources
     */
    override fun close() {
        timer.cancel()

        retryConnection = false
        module?.endConnection()
        module = null

        Handler(Looper.getMainLooper()).post {
            zmqContextTerminated = true
            zContext.close()
        }
    }

    private fun cycleModule() {
        module?.endConnection()

        if (zmqContextTerminated) {
            throw IllegalStateException("$TAG: Module cycle requested on a dead context instance")
        }

        module = MatrixTransceiveModule(
            connectionString,
            zContext,
            transceiverListener,
            installationId
        ).apply { start() }
    }

    companion object {
        val TAG = TransceiveManager::class.simpleName
        private val RETRY_BACKOFF_DELAYS = longArrayOf(50, 100, 200, 400, 700)

        /**
         * Amount of cycling that should be tried before declaring the connection dead
         */
        private val MAXIMUM_CYCLE_COUNT = RETRY_BACKOFF_DELAYS.size
    }

    private val transceiverListener = object : MatrixTransceiverListener {
        /**
         * Called if the run thread of the module has exited
         */
        override fun moduleStopped(hadTimeout: Boolean) {
            val retryCurrentLimitOk = currentCycleIteration < MAXIMUM_CYCLE_COUNT
            val retryOverallLimitOk = overallConnectionAttempts++ < 100
            if (retryConnection && hadTimeout && retryCurrentLimitOk && retryOverallLimitOk) {
                module = null
                Log.i(TAG, "Timeout; scheduling retry")

                timer.schedule(RETRY_BACKOFF_DELAYS[currentCycleIteration++]) {
                    initiateConnection()
                }
            } else {
                connectionStatusListener.onRetryLimitReached(matrix)
                close()
            }
        }

        override fun matrixShutDown() {
            connectionStatusListener.onMatrixShutdown(matrix)
        }

        override val nextOutMessage: Message?
            get() = outBox.poll()

        override fun onMatrixAcceptedConnection(width: Int, height: Int) {
            connectionRequestTask?.cancel()
            currentCycleIteration = 0
            connectionStatusListener.onConnectionRequestResponse(matrix, true)
        }

        override fun onReceive(message: JsonElement) {
            listener.onMessage(message)
        }
    }

    /**
     * Get an LedMatrix object containing all known information about the matrix the MessageSender
     * is currently connected to
     */
    override val currentMatrix: LedMatrix
        get() = matrix

    /**
     * Send a script command
     */
    override fun sendScriptData(scriptCommand: ScriptCommand) {
        sendMessage(ScriptData(scriptCommand))
    }

    /**
     * Request the listed script
     * @param scriptName name of the script
     */
    override fun requestScript(scriptName: String) {
        sendMessage(ScriptLoadRequest(scriptName))
    }
}