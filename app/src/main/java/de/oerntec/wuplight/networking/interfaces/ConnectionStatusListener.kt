package de.oerntec.wuplight.networking.interfaces

import de.oerntec.wuplight.networking.LedMatrix

interface ConnectionStatusListener {
    fun onConnectionRequestResponse(matrix: LedMatrix, granted: Boolean)

    /**
     * Gets called before the matrix has gracefully shut down
     */
    fun onMatrixShutdown(matrix: LedMatrix)

    /**
     * Gets called when we can't reach the matrix anymore
     */
    fun onRetryLimitReached(matrix: LedMatrix)

    fun onConnectionRequestTimeout(matrix: LedMatrix)
}