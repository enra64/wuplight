package de.oerntec.wuplight.networking.interfaces

import kotlinx.serialization.json.JsonElement


/**
 * This listener interface must be implemented to be notified of data sent by the matrix
 */
interface ScriptListener {
    /**
     * The script the fragment wants the server to load.
     * @return the name of the script minus ".py" as well as the name of the contained class. "null" is reserved.
     */
    fun requestScript(): String

    /**
     * Called whenever this script receives data from the matrix
     */
    fun onMessage(data: JsonElement)
}