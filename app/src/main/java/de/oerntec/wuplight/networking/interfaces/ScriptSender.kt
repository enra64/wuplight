package de.oerntec.wuplight.networking.interfaces

import de.oerntec.wuplight.networking.LedMatrix
import de.oerntec.wuplight.networking.ScriptCommand

interface ScriptSender {
    /**
     * Get an LedMatrix object containing all known information about the matrix the MessageSender
     * is currently connected to
     */
    val currentMatrix: LedMatrix

    /**
     * Send a script command
     */
    fun sendScriptData(scriptCommand: ScriptCommand)

    /**
     * Request the listed script
     * @param scriptName name of the script
     */
    fun requestScript(scriptName: String)
}