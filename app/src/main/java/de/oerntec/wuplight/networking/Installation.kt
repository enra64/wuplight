package de.oerntec.wuplight.networking

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import de.oerntec.wuplight.R
import java.util.*
import javax.inject.Inject

class Installation @Inject constructor(@ApplicationContext context: Context) {
    private val id: String

    init {
        id = loadId(context)
    }

    fun getId(): String {
        return id
    }

    private fun loadId(context: Context): String {
        val sharedPreferences = sharedPreferences(context)
        val installationId = sharedPreferences.getString("install_id", null)

        return if (installationId == null) {
            val newId = UUID.randomUUID().toString()
            with(sharedPreferences.edit()) {
                putString("install_id", newId)
                commit()
            }
            newId
        } else {
            installationId
        }
    }

    private fun sharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            context.getString(R.string.installation_shared_preferences_file),
            Context.MODE_PRIVATE
        )
    }
}