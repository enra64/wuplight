package de.oerntec.wuplight.networking.discovery

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.util.Log
import de.oerntec.wuplight.networking.LedMatrix

class NsdDiscoveryListener(
    private val context: Context,
    private val discoveryListener: DiscoveryListener
) : NsdManager.DiscoveryListener {
    /**
     * Called as soon as service discovery begins.
     */
    override fun onDiscoveryStarted(regType: String) {
        Log.d(TAG, "Service discovery started")
    }

    override fun onServiceFound(foundService: NsdServiceInfo) {
        // A service was found! Do something with it.
        Log.d(TAG, "Service discovery: $foundService")
        if (isLedMatrix(foundService)) {
            nsdManager(context).resolveService(foundService, object : NsdManager.ResolveListener {
                override fun onResolveFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
                    Log.e(TAG, "Service resolution failed for $serviceInfo; Err$errorCode")
                }

                override fun onServiceResolved(service: NsdServiceInfo?) {
                    service?.let {
                        Log.i(TAG, "Resolved $service")
                        safeLedMatrixConstruction(service)?.let(discoveryListener::foundMatrix)
                    }
                }
            })
        }
    }

    override fun onServiceLost(service: NsdServiceInfo) {
        if (isLedMatrix(service)) {
            safeLedMatrixConstruction(service)?.let(discoveryListener::lostMatrix)
        }
    }

    override fun onDiscoveryStopped(serviceType: String) {
        Log.d(TAG, "Discovery stopped: $serviceType")
    }

    override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
//        Log.e(TAG, "Discovery failed: Error code: $errorCode")
    }

    override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
//        Log.e(TAG, "Discovery failed: Error code: $errorCode")
    }

    companion object {
        private val TAG = NsdDiscoveryListener::class.simpleName
        private const val SERVICE_TYPE: String = "_iot-ledmatrix._tcp."

        fun startDiscovery(context: Context, discoveryListener: NsdDiscoveryListener) {
            nsdManager(context).discoverServices(
                SERVICE_TYPE,
                NsdManager.PROTOCOL_DNS_SD,
                discoveryListener
            )
        }

        fun stopDiscovery(context: Context, discoveryListener: NsdDiscoveryListener) {
            try {
                nsdManager(context).stopServiceDiscovery(discoveryListener)
            } catch (e: IllegalArgumentException) {
                Log.i(TAG, "Couldn't stopServiceDiscovery - listener not registered")
            }
        }

        private fun isLedMatrix(service: NsdServiceInfo): Boolean {
            return service.serviceType == SERVICE_TYPE
        }

        private fun safeLedMatrixConstruction(service: NsdServiceInfo): LedMatrix? {
            return try {
                val serviceNameParts = service.serviceName.split(":")
                if (serviceNameParts.size != 3) {
                    throw IllegalArgumentException("Bad service name ${service.serviceName}, couldn't split into name:width:height")
                }

                LedMatrix(
                    name = serviceNameParts[0],
                    dataPort = service.port,
                    address = service.host.hostAddress,
                    width = serviceNameParts[1].toInt(),
                    height = serviceNameParts[2].toInt()
                )
            } catch (e: Exception) {
                Log.w(TAG, e)
                // todo: show user
                null
            }
        }

        private fun nsdManager(context: Context): NsdManager {
            return context.getSystemService(Context.NSD_SERVICE) as NsdManager
        }
    }
}
