package de.oerntec.wuplight.networking

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
sealed class Message {
    @Transient
    abstract val message_type: String
    var id: String = ""
}

@Serializable
@SerialName("connection_request")
class ConnectionRequest : Message() {
    override val message_type: String = "connection_request"
}

@Serializable
@SerialName("connection_request_response")
class ConnectionRequestResponse(
    val matrix_width: Int,
    val matrix_height: Int,
    val granted: Boolean
) : Message() {
    override val message_type: String = "connection_request_response"
}


@Serializable
@SerialName("wakeuplight_ack")
class WakeUpLightAck(
    val msg_identifier: String
) : Message() {
    override val message_type: String = "wakeuplight_ack"
}

@Serializable
@SerialName("script_load_request")
class ScriptLoadRequest(
    val requested_script: String
) : Message() {
    override val message_type: String = "script_load_request"
}

@Serializable
@SerialName("script_data")
class ScriptData(
    val script_data: ScriptCommand
) : Message() {
    override val message_type: String = "script_data"
}

@Serializable
sealed class ScriptCommand {
    abstract val command: String
}

@Serializable
@SerialName("wakeuplight_set_time")
class WupLightSetTime(
    val wake_hour: Int,
    val wake_minute: Int,
    val wake_timezone: String,
    val blend_duration: Int,
    val lower_color_temperature: Int,
    val upper_color_temperature: Int
) : ScriptCommand() {
    override val command: String = "wakeuplight_set_time"
}

@Serializable
@SerialName("clear_wakeup_time")
class WupLightClearTime(
) : ScriptCommand() {
    override val command: String = "clear_wakeup_time"
}

@Serializable
@SerialName("test_color_temperature")
class TestColorTemperature(
    val color_temperature: Int
) : ScriptCommand() {
    override val command: String = "test_color_temperature"
}