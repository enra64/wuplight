package de.oerntec.wuplight.networking.interfaces

import de.oerntec.wuplight.networking.Message

/**
 * Created by arne on 13.01.18.
 */
interface Connection {
    fun initiateConnection()

    fun sendMessage(message: Message)

    /**
     * Close the connection, free resources
     */
    fun close()
}