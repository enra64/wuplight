package de.oerntec.wuplight.networking.implementations

import android.util.Log
import de.oerntec.wuplight.BuildConfig
import de.oerntec.wuplight.networking.ConnectionRequestResponse
import de.oerntec.wuplight.networking.interfaces.MatrixTransceiverListener
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import org.zeromq.SocketType
import org.zeromq.ZContext
import org.zeromq.ZMQ
import org.zeromq.ZMQException
import zmq.ZError
import java.nio.channels.ClosedSelectorException

/**
 * This is a thread that has
 * - an input queue for messages from app to matrix
 * - a listener for messages from matrix to app
 */
internal class MatrixTransceiveModule(
    private val connectionString: String,
    context: ZContext,
    private val matrixTransceiverListener: MatrixTransceiverListener,
    installationId: String
) : Thread() {
    private val matrixListenTimeout = 100
    private val connectionTestInterval = 750L
    private val pingMessage = "{\"id\":\"$installationId\",\"message_type\":\"connection_test\"}"
    private val TAG = "MatTransceiver"
    private val socket = context.createSocket(SocketType.DEALER)
    private val connectionTester = ConnectionTester(this, connectionTestInterval)
    private val json = Json { encodeDefaults = true }

    private var hadTimeout = false
    private var continueRunning = true

    init {
        socket.iPv6 = true
    }

    private fun listenToApp() {
        val outMessage = matrixTransceiverListener.nextOutMessage

        outMessage?.let {
            val messageString = json.encodeToString(outMessage)
            send(messageString)
            Log.d(TAG, "Sent message: ${messageString}")
        }

        if (connectionTester.requirePing()) {
            send(pingMessage)
        }
    }

    private fun send(message: String) {
        try {
            if (!socket.send(message)) {
                if (BuildConfig.DEBUG) {
                    Log.w("matrixTrcv", "ZMQ didn't want to take my message $message :(")
                }
            }
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                Log.w("matrixTrcv", "could not send $message", e)
            }

            continueRunning = false
        }
    }

    private fun listenToMatrix() {
        val receivedString: String?

        try {
            receivedString = socket.recvStr()

            receivedString?.let {
                val received = Json.parseToJsonElement(receivedString)
                connectionTester.reset()

                when (received.jsonObject["message_type"]?.jsonPrimitive?.content) {
                    "connection_request_response" -> {
                        val response = Json.decodeFromJsonElement<ConnectionRequestResponse>(received)
                        if (response.granted) {
                            matrixTransceiverListener.onMatrixAcceptedConnection(
                                response.matrix_width,
                                response.matrix_height
                            )
                            connectionTester.start()
                        }
                    }
                    "shutdown_notification" -> {
                        continueRunning = false
                        matrixTransceiverListener.matrixShutDown()
                        return
                    }
                    "connection_test_response" -> {
                    }
                    else -> matrixTransceiverListener.onReceive(received)
                }
            }
        } catch (e: ClosedSelectorException) {
            continueRunning = false
        } catch (e: ZError.IOException) {
            if (BuildConfig.DEBUG)
                Log.w(this.javaClass.name, "ZError.IOException occurred!", e)
        } catch (e: ArrayIndexOutOfBoundsException) {
            if (BuildConfig.DEBUG)
                Log.w(this.javaClass.name, "Array index out of bounds!", e)
        } catch (e: ZMQException) {
            // if the context has been terminated before the receive call, ignore that exception
            // we also expect the continue running flag to be no longer set
            if (continueRunning || e.errorCode != ZMQ.Error.ETERM.code) {
                if (BuildConfig.DEBUG)
                    Log.w(this.javaClass.name, "ZMQException occurred!", e)
                continueRunning = false
            }
        }
    }

    fun onTimeout() {
        endConnection()
        hadTimeout = true
    }

    fun endConnection() {
        continueRunning = false
    }

    override fun run() {
        if (!socket.connect(connectionString)) {
            throw RuntimeException("Socket could not connect to $connectionString")
        }
        socket.receiveTimeOut = matrixListenTimeout
        socket.linger = 10

        while (continueRunning) {
            listenToApp()
            listenToMatrix()
        }
        matrixTransceiverListener.moduleStopped(hadTimeout)
    }
}