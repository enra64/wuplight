package de.oerntec.wuplight.networking.interfaces

import de.oerntec.wuplight.networking.Message
import kotlinx.serialization.json.JsonElement

/**
 * The listener for classes that want to react to state changes of a connection module
 */
internal interface MatrixTransceiverListener {
    /**
     * Called if the run thread of the module has exited
     */
    fun moduleStopped(hadTimeout: Boolean)
    fun matrixShutDown()
    val nextOutMessage: Message?
    fun onMatrixAcceptedConnection(width: Int, height: Int)
    fun onReceive(message: JsonElement)
}