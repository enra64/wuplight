package de.oerntec.wuplight.networking

import kotlinx.serialization.Serializable

@Serializable
data class LedMatrix(
    val name: String,
    val dataPort: Int,
    val address: String,
    val width: Int,
    val height: Int,
    var isDefaultMatrix: Boolean = false
)