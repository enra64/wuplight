package de.oerntec.wuplight.networking.discovery

import de.oerntec.wuplight.networking.LedMatrix

interface DiscoveryListener {
    fun foundMatrix(matrix: LedMatrix)
    fun lostMatrix(matrix: LedMatrix)
}