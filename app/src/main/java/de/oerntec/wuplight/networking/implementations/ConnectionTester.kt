package de.oerntec.wuplight.networking.implementations

internal class ConnectionTester(
    private val connection: MatrixTransceiveModule,
    private val interval: Long
) {
    private var alive: Boolean
    private var killFlag: Boolean
    private var requirePing: Boolean

    fun start() {
        Thread {
            while (!killFlag) {
                if (alive) {
                    alive = false
                    requirePing = true
                    try {
                        Thread.sleep(interval)
                    } catch (ignored: InterruptedException) {
                    }
                } else {
                    // not a time out if the kill flag was set
                    if (!killFlag) {
                        connection.onTimeout()
                    }
                    break
                }
            }
        }.start()
    }

    fun requirePing(): Boolean {
        if (requirePing) {
            requirePing = false
            return true
        }
        return false
    }

    fun reset() {
        alive = true
    }

    fun kill() {
        killFlag = true
    }

    init {
        alive = true
        killFlag = false
        requirePing = true
    }
}