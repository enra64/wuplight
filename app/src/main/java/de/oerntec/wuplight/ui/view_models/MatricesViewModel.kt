package de.oerntec.wuplight.ui.view_models

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import de.oerntec.wuplight.data.repositories.DefaultWupLightRepository
import de.oerntec.wuplight.networking.LedMatrix
import de.oerntec.wuplight.networking.discovery.DiscoveryListener
import de.oerntec.wuplight.networking.discovery.NsdDiscoveryListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import javax.inject.Inject


@SuppressLint("StaticFieldLeak")  // application context is unproblematic here
@HiltViewModel
class MatricesViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val repository: DefaultWupLightRepository
) :
    ViewModel(),
    DiscoveryListener {

    private val matrices: MutableList<LedMatrix> = mutableListOf()
    private val defaultWupLight = MutableLiveData<LedMatrix?>()
    private val liveMatrices = MutableLiveData<List<LedMatrix>>()
    private val nsdDiscoveryListener = NsdDiscoveryListener(context, this)

    init {
        defaultWupLight.value = repository.getDefaultWupLight()
    }

    fun startDiscovery() {
        NsdDiscoveryListener.startDiscovery(
            context,
            nsdDiscoveryListener
        )
    }

    fun stopDiscovery() {
        NsdDiscoveryListener.stopDiscovery(
            context,
            nsdDiscoveryListener
        )
    }

    fun getDefaultWupLight() : MutableLiveData<LedMatrix?> {
        return defaultWupLight
    }

    fun setDefaultWupLight(matrix: LedMatrix) {
        matrix.isDefaultMatrix = true
        repository.saveDefaultWupLight(matrix)
        defaultWupLight.value = matrix

        liveMatrices.value =
            liveMatrices.value?.map { it.copy(isDefaultMatrix = it.address == matrix.address) }
    }

    override fun onCleared() {
        super.onCleared()
        stopDiscovery()
    }

    fun liveMatrices(): LiveData<List<LedMatrix>> {
        return liveMatrices
    }

    suspend fun isDefaultMatrixReachable(): Boolean {
        return defaultWupLight.value?.let { defaultMatrix ->
            val socketAddress = InetSocketAddress(defaultMatrix.address, 22)
            withContext(Dispatchers.IO) {
                try {
                    val sock = Socket()
                    sock.connect(socketAddress, 2000)
                    sock.close()
                    true
                } catch (e: IOException) {
                    false
                }
            }
        } ?: run {
            false
        }
    }

    override fun foundMatrix(matrix: LedMatrix) {
        Log.i("MVM", "found matrix $matrix")
        if (matrix.address == defaultWupLight.value?.address) {
            matrix.isDefaultMatrix = true
        }

        val unknownMatrix = matrices.find { it.address == matrix.address } == null

        if (unknownMatrix) {
            matrices.add(matrix)
            liveMatrices.postValue(matrices)
        }
    }

    override fun lostMatrix(matrix: LedMatrix) {
        Log.i("MVM", "lost matrix $matrix")
        matrices.removeIf { it.address == matrix.address }
        liveMatrices.value = matrices
    }
}