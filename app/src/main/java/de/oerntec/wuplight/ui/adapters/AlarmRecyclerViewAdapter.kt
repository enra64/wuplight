package de.oerntec.wuplight.ui.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import de.oerntec.wuplight.R
import de.oerntec.wuplight.data.objs.Alarm

class AlarmRecyclerViewAdapter(
    private val onAlarmClick: (Alarm) -> Unit,
    private val onActiveToggle: (Alarm, Boolean) -> Unit
) : RecyclerView.Adapter<AlarmRecyclerViewAdapter.AlarmViewHolder>() {
    private var alarms: List<Alarm>? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rv_item_alarm, parent, false)
        return AlarmViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlarmViewHolder, position: Int) {
        alarms?.let { list ->
            val alarm = list[position]
            val context = holder.itemView.context
            alarm.apply {
                holder.time.text = context.getString(R.string.alarm_full_time, hour, minute)
                holder.colorRange.text = context.getString(R.string.alarm_list_color_temperature_range, startColorTemperature, endColorTemperature)
                holder.leadTime.text = context.getString(R.string.alarm_list_lead_time, blendInDuration)
                holder.switch.setOnCheckedChangeListener(null) // avoid getting called for this
                holder.switch.isChecked = isActive
            }

            holder.itemView.setOnClickListener { onAlarmClick(alarm) }
            holder.switch.setOnCheckedChangeListener { _, isChecked ->
                onActiveToggle(
                    alarm,
                    isChecked
                )
            }
        }
    }

    fun setData(alarms: List<Alarm>) {
        this.alarms = alarms
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return alarms!!.let { it[position].id.hashCode().toLong() }
    }

    override fun getItemCount(): Int = alarms?.size ?: 0

    inner class AlarmViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val time: TextView = view.findViewById(R.id.time)
        val leadTime: TextView = view.findViewById(R.id.lead_time)
        val colorRange: TextView = view.findViewById(R.id.color_temperature_range)
        val switch: SwitchCompat = view.findViewById(R.id.active)

        override fun toString(): String {
            return "${super.toString()} '${colorRange.text}, ${leadTime.text}, isEnabled: ${switch.isChecked}'"
        }
    }
}