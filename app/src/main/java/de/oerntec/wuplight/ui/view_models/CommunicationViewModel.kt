package de.oerntec.wuplight.ui.view_models

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import de.oerntec.wuplight.networking.Installation
import de.oerntec.wuplight.networking.LedMatrix
import de.oerntec.wuplight.networking.ScriptCommand
import de.oerntec.wuplight.networking.implementations.TransceiveManager
import de.oerntec.wuplight.networking.interfaces.ConnectionStatusListener
import de.oerntec.wuplight.networking.interfaces.ScriptListener
import kotlinx.serialization.json.JsonElement
import javax.inject.Inject

enum class ConnectionStatus {
    READY_TO_START,
    CONNECTING,
    CONNECTED,
    MATRIX_SHUT_DOWN,
    CONNECTION_LOST,
    CONNECTION_REQUEST_NO_ANSWER,
    CONNECTION_REQUEST_REJECTED
}

@SuppressLint("StaticFieldLeak")
@HiltViewModel
class CommunicationViewModel @Inject constructor(@ApplicationContext private val context: Context) :
    ViewModel() {
    val connectionStatus = MutableLiveData(ConnectionStatus.READY_TO_START)
    private var transceiveManager: TransceiveManager? = null
    private var scriptListener: ScriptListener? = null

    fun start(targetMatrix: LedMatrix, scriptListener: ScriptListener) {
        when (connectionStatus.value) {
            ConnectionStatus.READY_TO_START -> {
                transceiveManager =
                    TransceiveManager(
                        Installation(context).getId(),
                        targetMatrix,
                        scriptListenWrapper,
                        connectionStatusListener
                    )
                transceiveManager!!.initiateConnection()

                Log.d(TAG, "Starting connection request")
                connectionStatus.value = ConnectionStatus.CONNECTING
                this.scriptListener = scriptListener
            }
            ConnectionStatus.CONNECTED -> {
                Log.i(TAG, "Already connected")
            }
            else -> throw IllegalStateException("Trying to start $TAG in state ${connectionStatus.value}")
        }
    }

    fun sendCommand(command: ScriptCommand) {
        if (connectionStatus.value == ConnectionStatus.CONNECTED) {
            transceiveManager?.sendScriptData(command)
        } else {
            Log.w(
                TAG,
                "Trying to send command ${command.javaClass.name}, " +
                        "connection status ${connectionStatus.value}, " +
                        "to ${transceiveManager?.currentMatrix}"
            )
        }
    }

    fun closeConnection() {
        transceiveManager?.close()
        transceiveManager = null
        connectionStatus.value = ConnectionStatus.READY_TO_START
    }

    override fun onCleared() {
        closeConnection()
    }

    private val scriptListenWrapper = object : ScriptListener {
        /**
         * The script the fragment wants the server to load.
         * @return the name of the script minus ".py" as well as the name of the contained class. "null" is reserved.
         */
        override fun requestScript(): String = "_WakeUpLight"

        /**
         * Called whenever this script receives data from the matrix
         */
        override fun onMessage(data: JsonElement) {
            scriptListener?.onMessage(data)
        }
    }

    private val connectionStatusListener = object : ConnectionStatusListener {
        override fun onConnectionRequestResponse(matrix: LedMatrix, granted: Boolean) {
            ifCorrectMatrix(matrix) {
                if (granted) {
                    Log.d(TAG, "Connection accepted")
                    connectionStatus.postValue(ConnectionStatus.CONNECTED)
                } else {
                    connectionStatus.postValue(ConnectionStatus.CONNECTION_REQUEST_REJECTED)
                }
            }
        }

        override fun onConnectionRequestTimeout(matrix: LedMatrix) {
            ifCorrectMatrix(matrix) {
                connectionStatus.postValue(ConnectionStatus.CONNECTION_REQUEST_NO_ANSWER)
            }
        }

        override fun onMatrixShutdown(matrix: LedMatrix) {
            ifCorrectMatrix(matrix) {
                connectionStatus.postValue(ConnectionStatus.MATRIX_SHUT_DOWN)
            }
        }

        override fun onRetryLimitReached(matrix: LedMatrix) {
            ifCorrectMatrix(matrix) {
                connectionStatus.postValue(ConnectionStatus.CONNECTION_LOST)
            }
        }

        private fun ifCorrectMatrix(checkMatrix: LedMatrix, then: (LedMatrix) -> Unit) {
            if (checkMatrix.address == transceiveManager?.currentMatrix?.address) {
                then(checkMatrix)
            }
        }
    }


    companion object {
        val TAG = "CommsVM"
    }
}