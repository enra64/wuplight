package de.oerntec.wuplight.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.jem.rubberpicker.RubberRangePicker
import com.tomerrosenfeld.customanalogclockview.HandsOverlay
import dagger.hilt.android.AndroidEntryPoint
import de.oerntec.wuplight.MainActivity
import de.oerntec.wuplight.R
import de.oerntec.wuplight.data.objs.Alarm
import de.oerntec.wuplight.ui.view_models.AlarmsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_alarm_config.*
import java.util.*

@AndroidEntryPoint
class AlarmConfigFragment : Fragment() {
    private val viewModel: AlarmsViewModel by navGraphViewModels(R.id.nav_graph) { defaultViewModelProviderFactory }
    private val defaultAlarm = Alarm(
        hour = 8,
        minute = 0,
        startColorTemperature = 1400,
        endColorTemperature = 5500,
        blendInDuration = 30,
        isActive = false,
        wasSetActiveAt = 0
    )
    private var alarm: Alarm = defaultAlarm
    private var deleteFlag = false
    private var saveFlag = false
    private val toolbar: Toolbar
        get() = (activity as MainActivity).toolbar

    companion object {
        fun newInstance(alarm: Alarm?) = AlarmConfigFragment().apply {
            arguments = bundleOf("alarm" to alarm)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        alarm = arguments?.getParcelable("alarm") ?: defaultAlarm
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(alarm != defaultAlarm)
        return inflater.inflate(R.layout.fragment_alarm_config, container, false)
    }

    private fun showTimePicker() {
        val picker = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .setHour(alarm.hour)
            .setMinute(alarm.minute)
            .setTitleText(R.string.select_alarm_time)
            .build()
        picker.addOnPositiveButtonClickListener {
            alarm = alarm.copy(hour = picker.hour, minute = picker.minute)
            updateAlarmConfigClock(alarm)
        }
        picker.show(parentFragmentManager, "alarm${alarm.id}config")
    }

    private fun updateAlarmConfigClock(alarm: Alarm) {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR, alarm.hour)
        cal.set(Calendar.MINUTE, alarm.minute)
        alarm_config_analog_clock.setTime(cal)
        val alarmHourFormatted = alarm.hour.toString().padStart(2, '0')
        val alarmMinuteFormatted = alarm.minute.toString().padStart(2, '0')
        alarm_config_digital_clock.text = "$alarmHourFormatted:$alarmMinuteFormatted"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        alarm_config_analog_clock.setOnClickListener { showTimePicker() }
        alarm_config_digital_clock.setOnClickListener { showTimePicker() }
        loadAnalogClockDrawables()
        updateAlarmConfigClock(alarm)

        temperature_picker.apply {
            setOnRubberRangePickerChangeListener(temperatureRangeListener)
            alarm.let {
                setCurrentStartValue(it.startColorTemperature)
                setCurrentEndValue(it.endColorTemperature)
            }
        }

        (requireActivity() as MainActivity).setToolbarTitle(null)
        (requireActivity() as MainActivity).showFab(false)
        (requireActivity() as MainActivity).setToolbarTranslucent(false)
    }

    private fun loadAnalogClockDrawables() {
        val face = ContextCompat.getDrawable(requireContext(), R.drawable.face)
        val hourHand = ContextCompat.getDrawable(requireContext(), R.drawable.hour_hand)
        val minuteHand = ContextCompat.getDrawable(requireContext(), R.drawable.minute_hand)

        val scale = .65f
        alarm_config_analog_clock.setScale(scale)
        alarm_config_analog_clock.setHandsOverlay(HandsOverlay(hourHand, minuteHand).withScale(scale))
        alarm_config_analog_clock.setFace(face)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_alarm_config, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        toolbar.menu.findItem(R.id.action_delete).setOnMenuItemClickListener {
            deleteFlag = true
            findNavController().navigateUp()
            true
        }
        toolbar.menu.findItem(R.id.action_save).setOnMenuItemClickListener {
            saveFlag = true
            findNavController().navigateUp()
            true
        }
        toolbar.menu.findItem(R.id.action_cancel).setOnMenuItemClickListener {
            findNavController().navigateUp()
            true
        }
    }

    override fun onPause() {
        super.onPause()
        alarm.let {
            if (deleteFlag) {
                viewModel.removeAlarm(it)
            } else if (saveFlag) {
                viewModel.changeAlarm(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).showFab(false)
    }

    private val temperatureRangeListener =
        object : RubberRangePicker.OnRubberRangePickerChangeListener {
            override fun onProgressChanged(
                rangePicker: RubberRangePicker,
                startValue: Int,
                endValue: Int,
                fromUser: Boolean
            ) {
                if (fromUser) {
                    alarm = alarm.copy(
                        startColorTemperature = startValue,
                        endColorTemperature = endValue
                    )
                }
            }

            override fun onStartTrackingTouch(
                rangePicker: RubberRangePicker,
                isStartThumb: Boolean
            ) = Unit

            override fun onStopTrackingTouch(
                rangePicker: RubberRangePicker,
                isStartThumb: Boolean
            ) = Unit

        }
}