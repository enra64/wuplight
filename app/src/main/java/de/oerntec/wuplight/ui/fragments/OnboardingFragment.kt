package de.oerntec.wuplight.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.navGraphViewModels
import androidx.viewpager2.widget.ViewPager2
import dagger.hilt.android.AndroidEntryPoint
import de.oerntec.wuplight.MainActivity
import de.oerntec.wuplight.R
import de.oerntec.wuplight.data.objs.OnboardingSlide
import de.oerntec.wuplight.ui.adapters.OnboardingAdapter
import de.oerntec.wuplight.ui.utils.hideSystemUI
import de.oerntec.wuplight.ui.utils.showSystemUI
import de.oerntec.wuplight.ui.view_models.MiscUserInfoViewModel
import kotlinx.android.synthetic.main.fragment_onboarding.*

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
@AndroidEntryPoint
class OnboardingFragment : Fragment() {
    private val miscUserViewModel by navGraphViewModels<MiscUserInfoViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }
    private val adapter = OnboardingAdapter(
        listOf(
            OnboardingSlide("Welcome", "Ease into your mornings with the power of the sun under your control!", "sunrise-animation.json"),
            OnboardingSlide("Pairing", "Before you have the power of the sun, we'll need you to find your device on the network.", "settings-animation.json")
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideSystemUI(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_onboarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onboarding_viewpager.adapter = adapter
        onboarding_viewpager.registerOnPageChangeCallback(onSlideChanged)
        onboarding_viewpager_indicator.setViewPager(onboarding_viewpager)
        (requireActivity() as MainActivity).showToolbar(false)
        (requireActivity() as MainActivity).showFab(false)
        (requireActivity() as MainActivity).setToolbarTranslucent(false)
    }

    private val onSlideChanged = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)

            if (position == adapter.itemCount - 1) {
                onboarding_button_next.setOnClickListener {
                    miscUserViewModel.setOnboardingDone()
                    requireView().findNavController().navigate(OnboardingFragmentDirections.actionOnboardingFragmentToAvailableWupLightsFragment())
                }
            } else {
                onboarding_button_next.setOnClickListener {
                    onboarding_viewpager.currentItem.let {
                        onboarding_viewpager.setCurrentItem(it + 1, true)
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        showSystemUI(requireActivity())
    }
}