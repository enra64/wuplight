@file:Suppress("DEPRECATION")

package de.oerntec.wuplight.ui.utils

import android.app.Activity
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_VISIBLE
import android.view.WindowManager
import de.oerntec.wuplight.MainActivity

fun hideSystemUI(activity: Activity) {
    activity.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    activity.window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            // Set the content to appear under the system bars so that the
            // content doesn't resize when the system bars hide and show.
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            //or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            // Hide the nav bar
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            // hide the status bar
            //or View.SYSTEM_UI_FLAG_FULLSCREEN
            )
}

/**
 * Shows the system bars by removing all the flags
 * except for the ones that make the content appear under the system bars.
 */
fun showSystemUI(activity: Activity) {
    activity.window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    (activity as MainActivity).showToolbar()
    activity.window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_VISIBLE
}