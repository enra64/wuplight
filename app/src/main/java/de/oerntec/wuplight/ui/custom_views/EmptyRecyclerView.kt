package de.oerntec.wuplight.ui.custom_views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class EmptyRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    private var _emptyView: View? = null
    var emptyView: View?
        get() = _emptyView
        set(value) {
            _emptyView = value
            checkIfEmpty()
        }

    private val observer: AdapterDataObserver = object : AdapterDataObserver() {
        override fun onChanged() = checkIfEmpty()
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) = checkIfEmpty()
        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) = checkIfEmpty()
    }

    fun checkIfEmpty() {
        emptyView?.let { emptyView ->
            adapter?.let { adapter ->
                val emptyViewVisible = adapter.itemCount == 0
                emptyView.visibility = if (emptyViewVisible) View.VISIBLE else View.GONE
                visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
            }
        }
    }

    override fun setAdapter(newAdapter: Adapter<*>?) {
        adapter?.unregisterAdapterDataObserver(observer)
        super.setAdapter(newAdapter)
        newAdapter?.registerAdapterDataObserver(observer)
        checkIfEmpty()
    }
}