package de.oerntec.wuplight.ui.view_models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.oerntec.wuplight.data.objs.Alarm
import de.oerntec.wuplight.data.repositories.AlarmRepository
import javax.inject.Inject

@HiltViewModel
class AlarmsViewModel @Inject constructor(
    private val repository: AlarmRepository
) : ViewModel() {
    private val liveAlarms = MutableLiveData<List<Alarm>>()

    fun changeAlarm(alarm: Alarm) {
        changeAlarms(listOf(alarm))
    }

    private fun changeAlarms(alarms: List<Alarm>) {
        alarms.forEach { repository.setAlarm(it) }
        val newAlarms = repository.getAlarms()
        liveAlarms.postValue(newAlarms)
    }

    fun toggleAlarm(alarm: Alarm, active: Boolean) {
        val toggledAlarms = getAlarms().value!!.map {
            if (it.id == alarm.id) {
                alarm.copy(isActive = active, wasSetActiveAt = System.currentTimeMillis())
            } else {
                it.copy(isActive = false, wasSetActiveAt = 0)
            }
        }
        changeAlarms(toggledAlarms)
    }

    fun removeAlarm(alarm: Alarm) {
        repository.removeAlarm(alarm.id)
        liveAlarms.value = repository.getAlarms()
    }

    fun getAlarms(): MutableLiveData<List<Alarm>> {
        liveAlarms.postValue(repository.getAlarms())
        return liveAlarms
    }
}