package de.oerntec.wuplight.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import de.oerntec.wuplight.R
import de.oerntec.wuplight.data.objs.OnboardingSlide

class OnboardingAdapter(private val slides: List<OnboardingSlide>) : RecyclerView.Adapter<OnboardingAdapter.OnboardingSlidesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnboardingSlidesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.onboarding_slide, parent, false)
        return OnboardingSlidesViewHolder(view)
    }

    override fun onBindViewHolder(holder: OnboardingSlidesViewHolder, position: Int) {
        holder.bind(slides[position])
    }

    override fun getItemCount(): Int {
        return slides.size
    }

    inner class OnboardingSlidesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val animation: LottieAnimationView = view.findViewById(R.id.onboarding_slide_animation)
        private val title: TextView = view.findViewById(R.id.onboarding_slide_title)
        private val description: TextView = view.findViewById(R.id.onboarding_slide_description)

        fun bind(slide: OnboardingSlide) {
            title.text = slide.title
            description.text = slide.description
            animation.imageAssetsFolder = "images"
            animation.setAnimation(slide.animation)
        }
    }
}