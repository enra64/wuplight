package de.oerntec.wuplight.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.oerntec.wuplight.MainActivity
import de.oerntec.wuplight.R
import de.oerntec.wuplight.data.objs.Alarm
import de.oerntec.wuplight.data.objs.alarmToCommand
import de.oerntec.wuplight.networking.LedMatrix
import de.oerntec.wuplight.networking.WakeUpLightAck
import de.oerntec.wuplight.networking.WupLightClearTime
import de.oerntec.wuplight.networking.interfaces.ScriptListener
import de.oerntec.wuplight.ui.adapters.AlarmRecyclerViewAdapter
import de.oerntec.wuplight.ui.view_models.*
import de.oerntec.wuplight.ui.view_models.ConnectionStatus.READY_TO_START
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_alarms.*
import kotlinx.coroutines.launch
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.decodeFromJsonElement
import java.util.*
import kotlin.concurrent.schedule

@AndroidEntryPoint
class AlarmsFragment : Fragment() {
    private val alarmsViewModel by navGraphViewModels<AlarmsViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }
    private val matricesViewModel by navGraphViewModels<MatricesViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }
    private val communicationViewModel by navGraphViewModels<CommunicationViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }
    private val miscUserInfoViewModel by navGraphViewModels<MiscUserInfoViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }
    private var acknowledgementTimer = Timer("AckTimer")
    private var latestAckTimerTask: TimerTask? = null

    private val alarmsAdapter by lazy {
        AlarmRecyclerViewAdapter(
            this@AlarmsFragment::onAlarmClick,
            this@AlarmsFragment::onAlarmToggle
        )
    }
    private val toolbar: Toolbar
        get() = (activity as AppCompatActivity).toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!miscUserInfoViewModel.isOnboardingDone()) {
            skipToOnboarding()
            (requireActivity() as MainActivity).showFab(false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_alarms, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        alarmsViewModel.getAlarms().observe(viewLifecycleOwner, {
            alarmsAdapter.setData(it)
        })
        (requireActivity() as MainActivity).setToolbarTranslucent(true)
    }

    override fun onResume() {
        super.onResume()
        matricesViewModel.getDefaultWupLight().value?.let(this@AlarmsFragment::attemptConnection)
        (requireActivity() as MainActivity).showFab(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_default, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        toolbar.menu.findItem(R.id.action_settings).setOnMenuItemClickListener {
            findNavController().navigate(
                AlarmsFragmentDirections.actionAlarmsFragmentToPreferencesFragment()
            )
            true
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(alarm_list) {
            layoutManager = LinearLayoutManager(context)
            adapter = alarmsAdapter
            emptyView = emptyAlarmsHint
        }
        with(requireActivity() as MainActivity) {
            setFabOnClickListener {
                findNavController().navigate(
                    AlarmsFragmentDirections.actionAlarmsFragmentToWakeUpConfig(null)
                )
            }
        }

        communicationViewModel.connectionStatus.observe(viewLifecycleOwner) {
            showStatusIfProblem(it)
        }
        (requireActivity() as MainActivity).setToolbarTitle(null)
    }

    private fun attemptConnection(defaultMatrix: LedMatrix) {
        viewLifecycleOwner.lifecycleScope.launch {
            val reachable = matricesViewModel.isDefaultMatrixReachable()

            if (reachable) {
                when (val connectionStatus = communicationViewModel.connectionStatus.value!!) {
                    READY_TO_START -> communicationViewModel.start(defaultMatrix, scriptListener)
                    else -> showStatusIfProblem(connectionStatus)
                }
            } else {
                (requireActivity() as MainActivity).showSnackbar("Can't see ${defaultMatrix.name} on network")
            }
        }
    }

    private fun showStatusIfProblem(status: ConnectionStatus) {
        when (status) {
            ConnectionStatus.CONNECTION_LOST -> (requireActivity() as MainActivity).showSnackbar("Lost WUPLight connection")
            ConnectionStatus.CONNECTION_REQUEST_NO_ANSWER -> (requireActivity() as MainActivity).showSnackbar("WUPLight not responding")
            ConnectionStatus.CONNECTION_REQUEST_REJECTED -> (requireActivity() as MainActivity).showSnackbar("WUPLight denied connection")
            else -> {
            }
        }
    }

    private fun onAlarmToggle(alarm: Alarm, active: Boolean) {
        matricesViewModel.getDefaultWupLight().value?.let {
            alarmsViewModel.toggleAlarm(alarm, active)
            val command = if (active) {
                alarmToCommand(alarm)
            } else {
                WupLightClearTime()
            }
            communicationViewModel.sendCommand(command)
            latestAckTimerTask = acknowledgementTimer.schedule(2000) {
                alarmsViewModel.toggleAlarm(alarm, !active)
                (requireActivity() as MainActivity).showSnackbar(getString(R.string.wuplight_ack_missing), getString(R.string.find_wuplight)) {
                    findNavController().navigate(AlarmsFragmentDirections.actionAlarmsFragmentToAvailableWupLightsFragment())
                }
            }
        } ?: run {
            (requireActivity() as MainActivity).showSnackbar(getString(R.string.pair_wuplight_to_set_alarm), getString(R.string.pair)) {
                findNavController().navigate(AlarmsFragmentDirections.actionAlarmsFragmentToAvailableWupLightsFragment())
            }
        }
    }

    private fun onAlarmClick(alarm: Alarm) {
        findNavController().navigate(
            AlarmsFragmentDirections.actionAlarmsFragmentToWakeUpConfig(alarm)
        )
    }

    private fun skipToOnboarding() {
        findNavController().navigate(AlarmsFragmentDirections.actionAlarmsFragmentToOnboardingFragment())
    }

    override fun onPause() {
        super.onPause()
        communicationViewModel.closeConnection()
    }

    private val scriptListener = object : ScriptListener {
        override fun requestScript(): String = "_WakeUpLight"
        override fun onMessage(data: JsonElement) {
            try {
                val ack = Json.decodeFromJsonElement<WakeUpLightAck>(data)
                latestAckTimerTask?.cancel()
                Log.d(TAG, "WUPLight ACK'd: ${ack.msg_identifier}")
            } catch (e: SerializationException) {
                Log.e(TAG, "Unexpected message $data")
            }
        }
    }

    companion object {
        const val TAG = "AlarmsFragment"

        @JvmStatic
        fun newInstance() = AlarmsFragment()
    }
}