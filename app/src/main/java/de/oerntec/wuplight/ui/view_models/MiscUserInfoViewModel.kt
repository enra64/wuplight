package de.oerntec.wuplight.ui.view_models

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.oerntec.wuplight.data.repositories.MiscUserRepository
import javax.inject.Inject

@HiltViewModel
class MiscUserInfoViewModel @Inject constructor(
    private val repository: MiscUserRepository
) : ViewModel() {
    fun isOnboardingDone(): Boolean {
        return repository.isOnboardingFinished()
    }

    fun setOnboardingDone() {
        repository.setOnboardingFinished()
    }
}