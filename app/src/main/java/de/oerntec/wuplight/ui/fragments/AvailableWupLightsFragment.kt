package de.oerntec.wuplight.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.oerntec.wuplight.MainActivity
import de.oerntec.wuplight.R
import de.oerntec.wuplight.ui.adapters.WupLightRecyclerViewAdapter
import de.oerntec.wuplight.ui.view_models.MatricesViewModel
import kotlinx.android.synthetic.main.fragment_wup_lights.*


@AndroidEntryPoint
class AvailableWupLightsFragment : Fragment() {
    private val discoveredMatricesViewModel by navGraphViewModels<MatricesViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }
    private val wupLightAdapter = WupLightRecyclerViewAdapter {
        discoveredMatricesViewModel.setDefaultWupLight(it)
        findNavController().navigateUp()
        (requireActivity() as MainActivity).showSnackbar("Paired with ${it.name}")
    }

    override fun onResume() {
        super.onResume()
        discoveredMatricesViewModel.startDiscovery()
        (requireActivity() as MainActivity).setToolbarHomeUpEnabled(true) { findNavController().navigateUp() }
        (requireActivity() as MainActivity).showFab(false)
    }

    override fun onPause() {
        super.onPause()
        discoveredMatricesViewModel.stopDiscovery()
        (requireActivity() as MainActivity).setToolbarHomeUpEnabled(false)
    }

    override fun onCreateView(li: LayoutInflater, vg: ViewGroup?, b: Bundle?): View? {
        return li.inflate(R.layout.fragment_wup_lights, vg, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(wupLightsRv) {
            emptyView = emptyWupLightsHint
            layoutManager = LinearLayoutManager(context)
            adapter = wupLightAdapter
        }
        (requireActivity() as MainActivity).setToolbarTitle(getString(R.string.available_wup_light_fragment_title))
        discoveredMatricesViewModel.liveMatrices().observe(
            viewLifecycleOwner,
            wupLightAdapter::setWupLightList
        )
        (requireActivity() as MainActivity).setToolbarTranslucent(false)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AvailableWupLightsFragment()
    }
}