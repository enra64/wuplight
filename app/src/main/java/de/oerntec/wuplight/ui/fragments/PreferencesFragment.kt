package de.oerntec.wuplight.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import dagger.hilt.android.AndroidEntryPoint
import de.oerntec.wuplight.MainActivity
import de.oerntec.wuplight.R
import de.oerntec.wuplight.ui.view_models.MatricesViewModel
import kotlinx.android.synthetic.main.fragment_preferences.*

@AndroidEntryPoint
class PreferencesFragment : Fragment() {
    private val matricesViewModel by navGraphViewModels<MatricesViewModel>(R.id.nav_graph) { defaultViewModelProviderFactory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_preferences, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        matricesViewModel.getDefaultWupLight().observe(viewLifecycleOwner) {
            it?.let {
                current_default_wup_light.text = it.name
            }
        }
        (requireActivity() as MainActivity).setToolbarTitle(getString(R.string.preferences_fragment_title))
        (requireActivity() as MainActivity).setToolbarTranslucent(false)
        default_wup_light_click_area.setOnClickListener { navigateWupLightSelector() }
        oss_licenses_click_area.setOnClickListener {
            startActivity(Intent(context, OssLicensesMenuActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).setToolbarHomeUpEnabled(true) { findNavController().navigateUp() }
        (requireActivity() as MainActivity).showFab(false)
    }

    override fun onPause() {
        super.onPause()
        (requireActivity() as MainActivity).setToolbarHomeUpEnabled(false)
    }

    private fun navigateWupLightSelector() {
        findNavController().navigate(
            PreferencesFragmentDirections.actionPreferencesFragmentToAvailableWupLightsFragment()
        )
    }

    companion object {
        @JvmStatic
        fun newInstance() = PreferencesFragment()
    }
}