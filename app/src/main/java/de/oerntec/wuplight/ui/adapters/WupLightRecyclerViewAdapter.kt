package de.oerntec.wuplight.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.oerntec.wuplight.R
import de.oerntec.wuplight.networking.LedMatrix

class WupLightRecyclerViewAdapter(
    private val wupLightClickedListener: (LedMatrix) -> Unit
) : RecyclerView.Adapter<WupLightRecyclerViewAdapter.WupLightItemVH>() {
    private var wupLights: List<LedMatrix>? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WupLightItemVH {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rv_item_wup_light, parent, false)
        return WupLightItemVH(view)
    }

    override fun onBindViewHolder(holder: WupLightItemVH, position: Int) {
        wupLights?.let { list ->
            val wupLight = list[position]
            holder.name.text = wupLight.name

            holder.itemView.setOnClickListener { wupLightClickedListener(wupLight) }

            holder.description.text = wupLight.address
        }
    }

    override fun getItemId(position: Int): Long {
        return wupLights!!.let { it[position].address.hashCode().toLong() }
    }

    override fun getItemCount(): Int = wupLights?.size ?: 0

    fun setWupLightList(wupLights: List<LedMatrix>?) {
        this.wupLights = wupLights
        notifyDataSetChanged()
    }

    inner class WupLightItemVH(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.name)
        val description: TextView = view.findViewById(R.id.description)

        override fun toString(): String {
            return "${super.toString()} '${description.text}'"
        }
    }
}