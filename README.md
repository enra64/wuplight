<p align="center">
  <img src="app/src/main/ic_launcher_anne-playstore.png" width="96">
</p>

# WUPLight
WUPLight is a personal app that is designed to communicate with LED matrices running my iot-ledmatrix framework. 
It does so by connecting to the WakeUpLight script using the ZMQ server running on the raspberry pi in the wake-up-light.

## Screenshots
Day Mode             |  Night Mode
:-------------------------:|:-------------------------:
<img src="assets/wupLightScreenshot1.png" width="250"> |  <img src="assets/wupLightScreenshot2.png" width="250">

## Setup
Visit the [iot-ledmatrix documentation](https://iot-ledmatrix.readthedocs.io/en/latest/) to create your own matrix!

## PlayStore
The app is available at [https://play.google.com/store/apps/details?id=de.oerntec.wuplight](https://play.google.com/store/apps/details?id=de.oerntec.wuplight).

<a href='https://play.google.com/store/apps/details?id=de.oerntec.wuplight&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
